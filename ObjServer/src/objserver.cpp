//##############################################################################
//
//	Test solution: asynchronous client-server for distributing of obj-files
//
//
//	Author: Gumega Andrii
//
//
//	2017-02
//
//##############################################################################

#include "..\include\objserver.h"
#include "..\include\objmessage.h"
#include <iostream>
#include <algorithm>

ObjServer::~ObjServer()
{
    try
    {
        if (!_isStopped)
        { this->stop();}

        WSACleanup();

        if (_TCPclients.size() > 0)
        { _TCPclients.clear(); }

        if (_objects.size() > 0)
        { _objects.clear(); }
    }
    catch (...)
    {
        std::string message = std::string("Error of server closing:");
        perror(message.c_str());
    }
}

/// Server Initialising
/// returns 'true' if initialising is successfully
bool ObjServer::init()
{
    std::cout << "Server Initialising..." << std::endl;

    // checking of objects
    if (_objects.size() == 0)
    {
        std::string answer;
        std::cout << "Objects are absent. Continue to start server? [Y/N]" << std::endl;
        std::cin >> answer;
        if (answer != "Y")
        {
            if (answer != "N")
            {
                std::cout << "Wrong command '" << answer << "'. ";
            }
            std::cout << "Server Inialising is stopped" << std::endl;
            return _isReady;
        }
    }

    std::cout << "Initialising Winsock..." << std::endl;
    if (WSAStartup(MAKEWORD(2,2), &_wsa) != 0)
    {
        std::cout << "Initialising Winsock is failed. Error Code: "
                  << WSAGetLastError() << std::endl;
        return _isReady;
    }
    std::cout << "Initialised." << std::endl;

    if((_listeningTCPsocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP )) == INVALID_SOCKET)
    {
        std::cout << "Socket creation is failed. Error Code: "
                  << WSAGetLastError() << std::endl;
        return _isReady;
    }
    std::cout << "Socket created." << std::endl;

    if (ioctlsocket(_listeningTCPsocket, FIONBIO, &_blocking) == SOCKET_ERROR)
    {
        std::cout << "Socket creation is failed. Error Code: "
                  << WSAGetLastError() << std::endl;
        return _isReady;
    }
    std::cout << "Socket set in non-blocking mode." << std::endl;

    _listeningSockAdr.sin_family = AF_INET;
    _listeningSockAdr.sin_addr.s_addr = INADDR_ANY;
    _listeningSockAdr.sin_port = htons(_port);

    if(bind(_listeningTCPsocket ,(struct sockaddr *)&_listeningSockAdr, sizeof(_listeningSockAdr)) == SOCKET_ERROR)
    {
        std::cout << "Socket binding is failed. Error Code: "
                  << WSAGetLastError() << std::endl;
        return _isReady;
    }
    std::cout << "Socket binding is done" << std::endl;

    _isReady = true;
    return _isReady;
}

/// Objects downloading
/// fills up '_objects' from filesystem by name
void ObjServer::downloadObjects(std::vector<std::string> &objects, const std::string& path)
{
    if (!_isStopped)
    {
        std::cout << "Server is running. Adding of object is blocked." << std::endl;
        return;
    }
    for (std::vector<std::string>::iterator it = objects.begin(); it < objects.end(); ++it)
    {
        try
        {
            _objects.insert(std::make_pair(*it,
                                           std::make_shared<ObjFromFileObject>(*it, path)));
        }
        catch (...)
        {
            std::string message = std::string("Error of downloading of object '")
                    + *it + std::string("':");
            perror(message.c_str());
        }
    }
    std::cout << "Objects are downloading in memory" << std::endl;
}

/// Start of server
void ObjServer::run()
{
    if (!_isReady)
    {
        std::cout << "Server is not ready to run. Please, init firstly" << std::endl;
        return;
    }

    _isStopped = false;

    if (listen(_listeningTCPsocket, SOMAXCONN) == SOCKET_ERROR)
    {
        std::cout << "Socket doesn't set to listen. Error Code: "
                  << WSAGetLastError() << std::endl;
        return;
    }
    
    FD_SET ListenSet;
    _TCPclients.clear();

    timeval timeout;
    timeout.tv_sec = 5;
    timeout.tv_usec = 0;

    sockaddr_in address;
    int addrlen = sizeof(sockaddr_in);

    while(!_isStopped)
    {
        FD_ZERO(&ListenSet);
        FD_SET(_listeningTCPsocket, &ListenSet);

        try
        {
        for(std::vector<SOCKET>::iterator it = _TCPclients.begin(); it != _TCPclients.end(); it++)
        { FD_SET(*it, &ListenSet); }

        if ((select(0, &ListenSet, NULL, NULL, &timeout)) == SOCKET_ERROR)
        {
            int error = WSAGetLastError();
            if (WSAEWOULDBLOCK != error)
            {
                std::cout << "Error of waiting event from any socket. Error Code: "
                          << error << std::endl;
            }
            continue;
        }

        if (FD_ISSET(_listeningTCPsocket, &ListenSet))
        {
            SOCKET sock = accept(_listeningTCPsocket, (sockaddr *)&address, (int *)&addrlen);
            if (sock == INVALID_SOCKET)
            {
                std::cout << "Error of accepting socket. Error Code: "
                                      << WSAGetLastError() << std::endl;
                continue;
            }

            if (ioctlsocket(sock, FIONBIO, &_blocking) == SOCKET_ERROR)
            {
                std::cout << "Error o listening socket. Error Code: "
                                      << WSAGetLastError() << std::endl;
                break;
            }

            _TCPclients.push_back(sock);

            if(!_isStopped && !_isProcessing)
            {
                _clientsThread = std::async(&ObjServer::clientProcessing, this);
                _isProcessing = true;
            }

            std::cout << "client: " << inet_ntoa(address.sin_addr) << ": "
                      << ntohs(address.sin_port) << " is added." << std::endl;
        }
        }
        catch(const std::exception &ex)
        {
            std::cout << "Error of server running: " << ex.what() << std::endl;
        }
        catch(...)
        {
            std::cout << "Error of server running. " << std::endl;
        }
    }
}

void ObjServer::clientProcessing()
{
    FD_SET ListenSet;

    typedef std::unordered_map<std::string, std::shared_ptr<ObjFromFileObject>>::iterator objectIter;

    const int bufLen = 1024;
    const int clientProcessing_sleepLoop_inSec = 50;
    char buf[bufLen];
    sockaddr_in address;
    int addrlen = sizeof(sockaddr_in);

    while(!_isStopped)
    {
        FD_ZERO(&ListenSet);
        FD_SET(_listeningTCPsocket, &ListenSet);

        for(std::vector<SOCKET>::iterator it = _TCPclients.begin(); it != _TCPclients.end(); it++)
        { FD_SET(*it, &ListenSet); }

        std::vector<SOCKET>::iterator it = _TCPclients.begin();
        while (it != _TCPclients.end())
        {
            if(FD_ISSET(*it, &ListenSet))
            {
                try
                {
                    getpeername(*it, (sockaddr*)&address , (int*)&addrlen);

                    memset(buf, 0, bufLen);
                    int bytes_read = recv(*it, buf, bufLen, 0);
                    if(bytes_read == SOCKET_ERROR)
                    {
                        int error = WSAGetLastError();
                        if (WSAECONNRESET == error)
                        {
                            closesocket(*it);
                            std::cout << "Connection reset by : " << inet_ntoa(address.sin_addr) << " : "
                                      << ntohs(address.sin_port) << std::endl;
                            it = _TCPclients.erase(it);
                            continue;
                        }
                        else if (WSAEWOULDBLOCK != error)
                        {
                            std::cout << "Receiving error " << error
                                      << " from : " << inet_ntoa(address.sin_addr) << " : "
                                      << ntohs(address.sin_port) << std::endl;
                        }
                        ++it;
                        continue;
                    }

                    std::cout << "Received from " << inet_ntoa(address.sin_addr) << " : "
                              << ntohs(address.sin_port) << " data: " << buf << std::endl;

                    ObjTcp::MessagePtr mess = reinterpret_cast<ObjTcp::MessagePtr>(&buf[0]);

                    if (ObjTcp::command_getObj == std::string(mess->command))
                    {
                        std::string object = mess->param1;
                        int udpport = atoi(mess->param2);
                        int size = 0;

                        objectIter ob = _objects.find(object);
                        if (ob != _objects.end())
                        {
                            size = (*ob).second.get()->size();
                        }
                        itoa(size, mess->param2, 10);

                        int bytes_send = send(*it, (char *)mess, sizeof(ObjTcp::Message), 0);
                        if (bytes_send == SOCKET_ERROR)
                        {
                            int error = WSAGetLastError();
                            if (WSAECONNRESET == error)
                            {
                                closesocket(*it);
                                std::cout << "Sending: Connection reset by : " << inet_ntoa(address.sin_addr) << " : "
                                          << ntohs(address.sin_port) << std::endl;
                                it = _TCPclients.erase(it);
                                continue;
                            }
                            else if (WSAEWOULDBLOCK != error)
                            {
                                std::cout << "Sending error " << error
                                          << " from : " << inet_ntoa(address.sin_addr) << " : "
                                          << ntohs(address.sin_port) << std::endl;
                            }
                        }
                        if (size > 0)
                        {
                            _sendingPool.push_back(std::thread(&ObjServer::objectSending, this, *it,
                                                               udpport, object));
                        }
                    }

                    ++it;
                }
                catch (std::exception& ex)
                {
                    std::cout << "Error of client processing: " << ex.what() << std::endl;
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(clientProcessing_sleepLoop_inSec));
    }
}

void ObjServer::objectSending(SOCKET clientNo, int clientPort, std::string object)
{
    const int bufLen = 16 * 1024;
    sockaddr_in address;
    int addrlen = sizeof(sockaddr_in);
    getpeername(clientNo, (sockaddr*)&address , (int*)&addrlen);
    address.sin_port = htons(clientPort);

    std::cout << "Sending of obj for " << inet_ntoa(address.sin_addr) << " : "
              << ntohs(address.sin_port) << std::endl;

    SOCKET s;
    if((s = socket(AF_INET , SOCK_DGRAM , IPPROTO_UDP)) == INVALID_SOCKET)
    {
        std::cout << "Socket creation to send obj is failed. Error Code: "
                  << WSAGetLastError() << std::endl;
        closesocket(s);
        return;
    }

    try
    {
        int objLength = _objects[object]->size();
        char* data = &_objects[object]->getObject()[0];
        int count = 0;
        while (count < objLength && !_isStopped)
        {
            if (sendto(s, data + count, ((count + bufLen) <= objLength) ? bufLen : (objLength - count),
                       0 , (sockaddr *) &address, addrlen) == SOCKET_ERROR)
            {
                std::cout << "sendto() failed with error code : %d" << WSAGetLastError();
                break;
            }
            count += bufLen;
        }
        if (count >= objLength)
        {
            std::cout << "Send obj " << object << " for " << inet_ntoa(address.sin_addr) << " : "
                      << ntohs(address.sin_port) << std::endl;
        }
    }
    catch (const std::exception &ex)
    {
        std::cout << "Error of sending data to " << inet_ntoa(address.sin_addr) << " : "
                  << ntohs(address.sin_port) << " : " << std::endl << ex.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "Error of sending data to " << inet_ntoa(address.sin_addr) << " : "
                  << ntohs(address.sin_port) << std::endl;
    }
    closesocket(s);
}

/// Stop of server
void ObjServer::stop()
{
    _isStopped = true;
    _isReady = false;

    // stop udp threads
    if (_sendingPool.size() > 0)
    {
        for (auto& poolItem : _sendingPool)
        {
            if (poolItem.joinable())
            { poolItem.join(); }
        }
        _sendingPool.clear();
    }

    if (_TCPclients.size() > 0)
    {
        for (SOCKET& s : _TCPclients)
        { closesocket(s); }
    }

    _clientsThread.wait();

    closesocket(_listeningTCPsocket);
}

bool ObjServer::isAlive()
{
    return _isStopped;
}
