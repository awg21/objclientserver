//##############################################################################
//
//	Test solution: asynchronous client-server for distributing of obj-files
//
//
//	Author: Gumega Andrii
//
//
//	2017-02
//
//##############################################################################

#include "..\include\objobject.h"
#include <cerrno>
#include <iostream>
#include <fstream>
#include <windows.h>

#pragma comment(lib,"Kernel32.lib")

ObjFromFileObject::ObjFromFileObject(const std::string &fileName, const std::string &path)
try
{
    std::string filePath = "";
    if ("" != path)
    {
        filePath = path;
    }
    filePath += fileName + std::string(".obj");

    std::ifstream objfile(filePath, std::ios::in);

    if (!objfile.is_open())
    {
        throw std::runtime_error("File is absent or can't be read");
    }

    objfile.seekg(0, std::ios::end);
    int fileSize = objfile.tellg();
    objfile.seekg(0, std::ios::beg);
    fileSize -= objfile.tellg();
    _object.resize(fileSize);
    objfile.read((char *)&(_object[0]), fileSize);
    objfile.close();

    _name = fileName;
}
catch (...)
{
    if (_object.size() > 0)
    { _object.clear(); }
    std::string message = std::string("Error of objfile reading: ") + fileName;
    perror(message.c_str());
    this->~ObjFromFileObject();
}

ObjFromFileObject::~ObjFromFileObject()
{
    if (_object.size() > 0)
    { _object.clear(); }

    _name.clear();
}

std::vector<char>& ObjFromFileObject::getObject()
{
    return _object;
}

std::string ObjFromFileObject::getName()
{
    return _name;
}

int ObjFromFileObject::size()
{
    return _object.size();
}
