//##############################################################################
//
//	Test solution: asynchronous client-server for distributing of obj-files
//
//
//	Author: Gumega Andrii
//
//
//	2017-02
//
//##############################################################################

#include <iostream>
#include <memory>
#include "..\include\objserver.h"
#include <future>
#include <string>

const std::string command_stop = "stop";

int main(int argc, char **argv)
{
    std::unique_ptr<ObjServer> server(new ObjServer());
    try
    {
        std::vector<std::string> objects = {"cube", "troop", "lamp", "shuttle", "demo"};
        server->downloadObjects(objects, std::string("..\\..\\data\\"));
        if (!server->init())
        {
            std::cout << "Program is stopped" << std::endl;
            return EXIT_FAILURE;
        }

        auto f = std::async(&ObjServer::run, server.get());

        bool isRunning = true;
        while(isRunning)
        {
            std::string command;
            std::cin >> command;
            if (command_stop == command)
            {
                isRunning = false;
                server->stop();
            }
        }

        f.wait();
        std::cout << "Server is stopped" << std::endl;
    }
    catch (...)
    {
        std::string message = std::string("Error of server execution:");
        perror(message.c_str());
    }

    return EXIT_SUCCESS;
}
