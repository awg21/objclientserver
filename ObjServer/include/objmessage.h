//##############################################################################
//
//	Test solution: asynchronous client-server for distributing of obj-files
//
//
//	Author: Gumega Andrii
//
//
//	2017-02
//
//##############################################################################

#ifndef OBJMESSAGE_H
#define OBJMESSAGE_H

namespace ObjTcp {

#include <string>

const std::string command_getObj = "getObj";

typedef struct
{
    char command[32];
    char param1[32];
    char param2[32];
} Message, *MessagePtr;

}

#endif // OBJMESSAGE_H
