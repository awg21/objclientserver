//##############################################################################
//
//	Test solution: asynchronous client-server for distributing of obj-files
//
//
//	Author: Gumega Andrii
//
//
//	2017-02
//
//##############################################################################

#ifndef OBJSERVER_H
#define OBJSERVER_H

#include "objobject.h"
#include <memory>
#include <future>
#include <mutex>
#include <unordered_map>
#include <vector>
#include <string>
#include <atomic>
#include <winsock2.h>

#pragma comment(lib,"ws2_32.lib")

class ObjServer
{
    const int _bufLen = 512;
    const int _defaultPort = 4444;
    u_long _blocking = 1;
    unsigned short _port;
    SOCKET _listeningTCPsocket;
    sockaddr_in _listeningSockAdr;
    WSADATA _wsa;
    bool _isReady = false;
    std::atomic_bool _isStopped = true, _isProcessing = false;
    std::vector<SOCKET> _TCPclients;
    std::future<void> _clientsThread;
    std::vector<std::thread> _sendingPool;
    std::unordered_map<std::string, std::shared_ptr<ObjFromFileObject>> _objects;

    void clientProcessing();
    void objectSending(SOCKET clientNo, int clientPort, std::string object);
public:
    ObjServer() : _port(_defaultPort) {}
    ObjServer(unsigned short port) : _port(port) {}

    bool init();
    void downloadObjects(std::vector<std::string> &objects, const std::string &path = "");
    void run();
    void stop();
    bool isAlive();

    ~ObjServer();
};

#endif // OBJSERVER_H
