//##############################################################################
//
//	Test solution: asynchronous client-server for distributing of obj-files
//
//
//	Author: Gumega Andrii
//
//
//	2017-02
//
//##############################################################################

#ifndef OBJOBJECT_H
#define OBJOBJECT_H

#include <vector>
#include <string>

class ObjFromFileObject
{
    std::vector<char> _object;
    std::string _name;
public:
    ObjFromFileObject(const std::string &fileName, const std::string& path = "");
    std::vector<char> &getObject();
    std::string getName();
    int size();

    ~ObjFromFileObject();
};

#endif // OBJOBJECT_H
