//###############################################################################
//
//	Test solution: asynchronous client-server for distributing of obj-files
//
//
//	Author: Gumega Andrii
//
//
//	2017-02
//
//#############################################################################


#include <iostream>
#include <memory>
#include <future>
#include "..\include\objclient.h"

int main(int argc, char* argv[])
{
    std::unique_ptr<ObjClient> client(new ObjClient());

    try
    {
        if (!client->init("127.0.0.1", 4444, 5555))
        {
            std::cout << "Program is stopped" << std::endl;
            return EXIT_FAILURE;
        }

        auto f = std::async(&ObjClient::run, client.get());

        bool isRunning = true;
        while(isRunning)
        {
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }

        f.wait();
        std::cout << "Client is stopped" << std::endl;
    }
    catch (...)
    {
        std::string message = std::string("Error of client execution:");
        perror(message.c_str());
    }

    return EXIT_SUCCESS;
}
