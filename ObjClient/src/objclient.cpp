//###############################################################################
//
//	Test solution: asynchronous client-server for distributing of obj-files
//
//
//	Author: Gumega Andrii
//
//
//	2017-02
//
//#############################################################################

#include "..\include\objclient.h"
#include "..\include\objmessage.h"

#include <iostream>
#include <thread>
#include <mutex>
#include <fstream>

ObjClient::ObjClient()
{
}

ObjClient::~ObjClient()
{
    try
    {
        if (!_isStopped)
        { this->stop();}

        WSACleanup();
    }
    catch (...)
    {
        std::string message = std::string("Error of server closing:");
        perror(message.c_str());
    }
}

bool ObjClient::init(std::string server_ip, int server_port, int udp_port)
{
    std::cout << "Client Initialising..." << std::endl;

    _udpport = udp_port;

    std::cout << "Initialising Winsock..." << std::endl;
    if (WSAStartup(MAKEWORD(2,2), &_wsa) != 0)
    {
        std::cout << "Initialising Winsock is failed. Error Code: "
                  << WSAGetLastError() << std::endl;
        return _isReady;
    }
    std::cout << "Initialised." << std::endl;

    if((_tcpSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP )) == SOCKET_ERROR)
    {
        std::cout << "Socket creation is failed. Error Code: "
                  << WSAGetLastError() << std::endl;
        return _isReady;
    }
    std::cout << "Socket created." << std::endl;

    memset((char *) &_tcpsadr, 0, sizeof(_tcpsadr));
    _tcpsadr.sin_family = AF_INET;
    _tcpsadr.sin_addr.S_un.S_addr = inet_addr(server_ip.c_str());
    _tcpsadr.sin_port = htons(server_port);

    _isReady = true;
    return _isReady;
}

void ObjClient::run()
{
    if(connect(_tcpSocket, (struct sockaddr *)&_tcpsadr, sizeof(_tcpsadr)) < 0)
    {
        std::cout << "Connection to remote server is failed. Error Code: "
                  << WSAGetLastError() << std::endl;
        return;
    }
    std::cout << "Connection to remote server is done" << std::endl;

    char* buf = new char[_bufLen];

    while(_isStopped)
    {
        std::string input;
        std::cout << "Print command: ";
        std::cin >> input;
        if (ObjTcp::command_getObj == input)
        {
            std::cout << "Print name of obj: ";
            std::cin >> input;

            SOCKET s;
            sockaddr_in sadr;
            if(INVALID_SOCKET == (s = socket(AF_INET , SOCK_DGRAM , IPPROTO_UDP)))
            {
                std::cout << "Could not create udp socket : " << WSAGetLastError() << std::endl;
                continue;
            }

            sadr.sin_family = AF_INET;
            sadr.sin_addr.s_addr = INADDR_ANY;
            sadr.sin_port = htons(_udpport);

            if(bind(s ,(struct sockaddr *)&sadr , sizeof(sadr)) == SOCKET_ERROR)
            {
                std::cout << "Bind failed with error code: " << WSAGetLastError() << std::endl;
                closesocket(s);
                continue;
            }

            ObjTcp::MessagePtr message = new ObjTcp::Message();
            strcpy(message->command, ObjTcp::command_getObj.c_str());
            strcpy(message->param1, input.c_str());
            memset(message->param2, '\0', 32);
            itoa(_udpport, message->param2, 10);

            if (send(_tcpSocket, (char *)message, sizeof(ObjTcp::Message), 0) == SOCKET_ERROR)
            {
                std::cout << "send() failed with error code: " << WSAGetLastError() << std::endl;
                closesocket(s);
                continue;
            }

            memset(buf, '\0', _bufLen);

            if (recv(_tcpSocket, buf, _bufLen, 0) == SOCKET_ERROR)
            {
                std::cout << "recv() failed with error code: " << WSAGetLastError() << std::endl;
                closesocket(s);
                continue;
            }

            message = reinterpret_cast<ObjTcp::MessagePtr>(&buf[0]);

            if (ObjTcp::command_getObj != std::string(message->command))
            {
                std::cout << "server answer doesn't return getObj" << std::endl;
                closesocket(s);
                continue;
            }

            int size = atoi(message->param2);
            if (0 == size)
            {
                std::cout << "Object '" << input << "' is absent on server"<< std::endl;
                closesocket(s);
                continue;
            }

            std::thread t(&ObjClient::receiveData, this, &s, &sadr, size, input);

            if (t.joinable())
            { t.join(); }
            closesocket(s);
        }
        else if (command_stop == input)
        {
            stop();
        }
    }
}

void ObjClient::stop()
{
    _isStopped = true;

    closesocket(_tcpSocket);
    _isReady = false;
}

void ObjClient::receiveData(SOCKET *s, sockaddr_in *sadr, int length, std::string object)
{
    char *data = new char[length];
    int count = 0, recv_len = 0;
    int adrlen = sizeof(*sadr);

    while(count < length)
    {
        if ((recv_len = recvfrom(*s, data + count,
                                 (((count + _dataBufLen) <= length) ? _dataBufLen : (length - count)), 0,
                                 (struct sockaddr *)sadr, &adrlen)) == SOCKET_ERROR)
        {
            std::cout << "recvfrom() failed with error code: " << WSAGetLastError() << std::endl;
            continue;
        }
        count += recv_len;
    }

    if (count == length)
    {
        // mutex to protect file access (shared across threads)
        static std::mutex mutex;
        std::lock_guard<std::mutex> lock(mutex);

        std::string filename = "downloaded_" + object + ".obj";
        std::ofstream objfile(filename.c_str());
        if (!objfile.is_open())
        {
            std::cout << "Error of creation file";
        }
        objfile << data;
        std::cout << "Data is saved in file: " << filename << std::endl;
    }
    else
    {
        std::cout << "data is not received or crashed" << std::endl;
    }

    delete[] data;
}

bool ObjClient::isAlive()
{
    return _isStopped;
}
