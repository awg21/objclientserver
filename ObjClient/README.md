###############################################################################
#
#	Test solution: asynchronous client-server for distributing of obj-files
#
#
#	Author: Gumega Andrii
#
#
#	2017-02
#
###############################################################################

Commands:

* take obj file from server: "getObj"

* stop client: "stop"