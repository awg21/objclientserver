//###############################################################################
//
//	Test solution: asynchronous client-server for distributing of obj-files
//
//
//	Author: Gumega Andrii
//
//
//	2017-02
//
//#############################################################################

#ifndef OBJCLIENT_H
#define OBJCLIENT_H

#include <string>
#include <atomic>
#include<winsock2.h>
#pragma comment(lib,"ws2_32.lib")

class ObjClient
{
    const int _bufLen = 512;
    const int _dataBufLen = 16 * 1024;
    const std::string command_stop = "stop";

    WSADATA _wsa;
    SOCKET _tcpSocket;
    struct sockaddr_in _tcpsadr;
    int _tcpsadrlen;
    int _udpport;

    bool _isReady = false;
    std::atomic_bool _isStopped = true;
public:
    ObjClient();
    ~ObjClient();

    bool init(std::string server_ip, int server_port, int udp_port);
    void run();
    void stop();
    void receiveData(SOCKET* s, sockaddr_in* sadr, int length, std::string object);
    bool isAlive();
};

#endif // OBJCLIENT_H
